require 'test_helper'

class GenericManagersControllerTest < ActionController::TestCase
  setup do
    @generic_manager = generic_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:generic_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create generic_manager" do
    assert_difference('GenericManager.count') do
      post :create, generic_manager: { comment: @generic_manager.comment, generic_id: @generic_manager.generic_id, score: @generic_manager.score, user_id: @generic_manager.user_id }
    end

    assert_redirected_to generic_manager_path(assigns(:generic_manager))
  end

  test "should show generic_manager" do
    get :show, id: @generic_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @generic_manager
    assert_response :success
  end

  test "should update generic_manager" do
    patch :update, id: @generic_manager, generic_manager: { comment: @generic_manager.comment, generic_id: @generic_manager.generic_id, score: @generic_manager.score, user_id: @generic_manager.user_id }
    assert_redirected_to generic_manager_path(assigns(:generic_manager))
  end

  test "should destroy generic_manager" do
    assert_difference('GenericManager.count', -1) do
      delete :destroy, id: @generic_manager
    end

    assert_redirected_to generic_managers_path
  end
end
