require 'test_helper'

class AssessmentManagersControllerTest < ActionController::TestCase
  setup do
    @assessment_manager = assessment_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:assessment_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create assessment_manager" do
    assert_difference('AssessmentManager.count') do
      post :create, assessment_manager: { assessment_id: @assessment_manager.assessment_id, comment: @assessment_manager.comment, score: @assessment_manager.score, user_id: @assessment_manager.user_id }
    end

    assert_redirected_to assessment_manager_path(assigns(:assessment_manager))
  end

  test "should show assessment_manager" do
    get :show, id: @assessment_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @assessment_manager
    assert_response :success
  end

  test "should update assessment_manager" do
    patch :update, id: @assessment_manager, assessment_manager: { assessment_id: @assessment_manager.assessment_id, comment: @assessment_manager.comment, score: @assessment_manager.score, user_id: @assessment_manager.user_id }
    assert_redirected_to assessment_manager_path(assigns(:assessment_manager))
  end

  test "should destroy assessment_manager" do
    assert_difference('AssessmentManager.count', -1) do
      delete :destroy, id: @assessment_manager
    end

    assert_redirected_to assessment_managers_path
  end
end
