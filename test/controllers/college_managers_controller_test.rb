require 'test_helper'

class CollegeManagersControllerTest < ActionController::TestCase
  setup do
    @college_manager = college_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:college_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create college_manager" do
    assert_difference('CollegeManager.count') do
      post :create, college_manager: { collegeaand_id: @college_manager.collegeaand_id, comment: @college_manager.comment, score: @college_manager.score, user_id: @college_manager.user_id }
    end

    assert_redirected_to college_manager_path(assigns(:college_manager))
  end

  test "should show college_manager" do
    get :show, id: @college_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @college_manager
    assert_response :success
  end

  test "should update college_manager" do
    patch :update, id: @college_manager, college_manager: { collegeaand_id: @college_manager.collegeaand_id, comment: @college_manager.comment, score: @college_manager.score, user_id: @college_manager.user_id }
    assert_redirected_to college_manager_path(assigns(:college_manager))
  end

  test "should destroy college_manager" do
    assert_difference('CollegeManager.count', -1) do
      delete :destroy, id: @college_manager
    end

    assert_redirected_to college_managers_path
  end
end
