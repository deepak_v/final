class AssessmentManagersController < ApplicationController
  before_action :set_assessment_manager, only: [:show, :edit, :update, :destroy]

  # GET /assessment_managers
  # GET /assessment_managers.json
  def index
    @assessment_managers = AssessmentManager.all
    @assessments = Assessment.all
  end

  # GET /assessment_managers/1
  # GET /assessment_managers/1.json
  def show
  end

  # GET /assessment_managers/new
  def new
    @assessment_manager = AssessmentManager.new
    @assessment_managers = AssessmentManager.all
    @assessments = Assessment.all
    @users = User.all
  end

  # GET /assessment_managers/1/edit
  def edit
    @assessments = Assessment.all
    @assessment_managers = AssessmentManager.all
    @users = User.all
  end

  # POST /assessment_managers
  # POST /assessment_managers.json
  def create
    @assessment_manager = AssessmentManager.new(assessment_manager_params)

    respond_to do |format|
      if @assessment_manager.save
        format.html { redirect_to assessments_path, notice: 'Assessment Report was successfully created.' }
        format.json { render :show, status: :created, location: @assessment_manager }
      else
        format.html { render :new }
        format.json { render json: @assessment_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assessment_managers/1
  # PATCH/PUT /assessment_managers/1.json
  def update
    respond_to do |format|
      if @assessment_manager.update(assessment_manager_params)
        format.html { redirect_to assessments_path, notice: 'Assessment Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @assessment_manager }
      else
        format.html { render :edit }
        format.json { render json: @assessment_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assessment_managers/1
  # DELETE /assessment_managers/1.json
  def destroy
    @assessment_manager.destroy
    respond_to do |format|
      format.html { redirect_to assessment_managers_url, notice: 'Assessment Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assessment_manager
      @assessment_manager = AssessmentManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assessment_manager_params
      params.require(:assessment_manager).permit(:user_id, :assessment_id, :comment, :score)
    end
end
