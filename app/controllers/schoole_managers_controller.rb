class SchooleManagersController < ApplicationController
  before_action :set_schoole_manager, only: [:show, :edit, :update, :destroy]

  # GET /schoole_managers
  # GET /schoole_managers.json
  def index
    @schoole_managers = SchooleManager.all
    @schooles = Schoole.all
  end

  # GET /schoole_managers/1
  # GET /schoole_managers/1.json
  def show
  end

  # GET /schoole_managers/new
  def new
    @schoole_manager = SchooleManager.new
    @schoole_managers = SchooleManager.all
    @schooles = Schoole.all
    @users = User.all
  end

  # GET /schoole_managers/1/edit
  def edit
    @schooles = Schoole.all
    @schoole_managers = SchooleManager.all
    @users = User.all
  end

  # POST /schoole_managers
  # POST /schoole_managers.json
  def create
    @schoole_manager = SchooleManager.new(schoole_manager_params)

    respond_to do |format|
      if @schoole_manager.save
        format.html { redirect_to schooles_path, notice: 'Score was successfully created.' }
        format.json { render :show, status: :created, location: @schoole_manager }
      else
        format.html { render :new }
        format.json { render json: @schoole_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schoole_managers/1
  # PATCH/PUT /schoole_managers/1.json
  def update
    respond_to do |format|
      if @schoole_manager.update(schoole_manager_params)
        format.html { redirect_to schooles_path, notice: 'Score was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoole_manager }
      else
        format.html { render :edit }
        format.json { render json: @schoole_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schoole_managers/1
  # DELETE /schoole_managers/1.json
  def destroy
    @schoole_manager.destroy
    respond_to do |format|
      format.html { redirect_to schoole_managers_url, notice: 'Score was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoole_manager
      @schoole_manager = SchooleManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoole_manager_params
      params.require(:schoole_manager).permit(:user_id, :schoole_id, :comment, :score)
    end
end
