class Users::RegistrationsController < Devise::RegistrationsController
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_registration
      @registration = Registration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def generic_params
      params.require(:registration).permit(:email, :password, :password_confirmation, :department_name, :position)
    end
end