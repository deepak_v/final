json.array!(@generic_managers) do |generic_manager|
  json.extract! generic_manager, :id, :user_id, :generic_id, :comment, :score
  json.url generic_manager_url(generic_manager, format: :json)
end
