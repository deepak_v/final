json.array!(@schoole_managers) do |schoole_manager|
  json.extract! schoole_manager, :id, :user_id, :schoole_id, :comment, :score
  json.url schoole_manager_url(schoole_manager, format: :json)
end
