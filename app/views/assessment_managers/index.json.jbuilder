json.array!(@assessment_managers) do |assessment_manager|
  json.extract! assessment_manager, :id, :user_id, :assessment_id, :comment, :score
  json.url assessment_manager_url(assessment_manager, format: :json)
end
