# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151202011034) do

  create_table "assessment_managers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "assessment_id"
    t.text     "comment"
    t.decimal  "score"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "assessment_managers", ["assessment_id"], name: "index_assessment_managers_on_assessment_id"
  add_index "assessment_managers", ["user_id"], name: "index_assessment_managers_on_user_id"

  create_table "assessments", force: :cascade do |t|
    t.date     "one"
    t.string   "two"
    t.string   "three"
    t.string   "four"
    t.string   "text"
    t.date     "five"
    t.text     "six"
    t.text     "seven"
    t.text     "eight"
    t.text     "nine"
    t.text     "ten"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "assessments", ["user_id"], name: "index_assessments_on_user_id"

  create_table "college_managers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "collegeaand_id"
    t.text     "comment"
    t.decimal  "score"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "college_managers", ["collegeaand_id"], name: "index_college_managers_on_collegeaand_id"
  add_index "college_managers", ["user_id"], name: "index_college_managers_on_user_id"

  create_table "collegeaands", force: :cascade do |t|
    t.string   "one"
    t.string   "two"
    t.string   "three"
    t.string   "four"
    t.string   "five"
    t.string   "six"
    t.date     "seven"
    t.date     "eight"
    t.text     "nine"
    t.text     "ten"
    t.text     "eleven"
    t.text     "twelve"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "collegeaands", ["user_id"], name: "index_collegeaands_on_user_id"

  create_table "generic_managers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "generic_id"
    t.text     "comment"
    t.decimal  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "generic_managers", ["generic_id"], name: "index_generic_managers_on_generic_id"
  add_index "generic_managers", ["user_id"], name: "index_generic_managers_on_user_id"

  create_table "generics", force: :cascade do |t|
    t.string   "one"
    t.string   "two"
    t.string   "three"
    t.string   "four"
    t.string   "five"
    t.string   "six"
    t.date     "seven"
    t.date     "eight"
    t.text     "nine"
    t.text     "ten"
    t.text     "eleven"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "generics", ["user_id"], name: "index_generics_on_user_id"

  create_table "schoole_managers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "schoole_id"
    t.text     "comment"
    t.decimal  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "schoole_managers", ["schoole_id"], name: "index_schoole_managers_on_schoole_id"
  add_index "schoole_managers", ["user_id"], name: "index_schoole_managers_on_user_id"

  create_table "schooles", force: :cascade do |t|
    t.string   "one"
    t.string   "two"
    t.string   "three"
    t.string   "four"
    t.string   "five"
    t.string   "six"
    t.date     "seven"
    t.date     "eight"
    t.text     "nine"
    t.text     "ten"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "department_name"
    t.string   "position"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
