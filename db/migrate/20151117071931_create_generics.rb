class CreateGenerics < ActiveRecord::Migration
  def change
    create_table :generics do |t|
      t.string :one
      t.string :two
      t.string :three
      t.string :four
      t.string :five
      t.string :six
      t.date :seven
      t.date :eight
      t.text :nine
      t.text :ten
      t.text :eleven
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
