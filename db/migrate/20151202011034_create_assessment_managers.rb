class CreateAssessmentManagers < ActiveRecord::Migration
  def change
    create_table :assessment_managers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :assessment, index: true, foreign_key: true
      t.text :comment
      t.decimal :score

      t.timestamps null: false
    end
  end
end
